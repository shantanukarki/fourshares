import hashlib
import cv2
import numpy as np

# imageName="emma_stone.bmp"
# imagePath = f"images\\{imageName}"
# encPath = f"encrypted\\enc_{imageName}"


# image = cv2.imread(imagePath,cv2.IMREAD_GRAYSCALE)
# encImg = cv2.imread(encPath,cv2.IMREAD_GRAYSCALE)

# image = cv2.imread(imagePath)
# encImg = cv2.imread(encPath)



def imgEncDec(img,secret_key):
    if len(img.shape)==2:
        height,width = img.shape
        img_type="G"
    else:
        img_type="C"
        height,width,s = img.shape


    N=3000+height*width
    h=0.01

    hash_obj = hashlib.sha256(secret_key.encode())
    seed = int.from_bytes(hash_obj.digest(), byteorder='big') % (2**32)
    np.random.seed(seed)

    x1 = np.random.rand(N)
    x2 = np.random.rand(N)
    x3 = np.random.rand(N)
    x4 = np.random.rand(N)
    x5 = np.random.rand(N)
    x6 = np.random.rand(N)
    x7 = np.random.rand(N)
    x8 = np.random.rand(N)

    for n in range(N-1):
        k1=10*(x2[n]-x1[n])+x4[n]
        j1=76*x1[n]-x1[n]*x3[n]+x4[n]
        r1=x1[n]*x2[n]-x3[n]-x4[n]+x7[n]
        t1=-3*(x1[n]+x2[n])+x5[n]
        a1=-x2[n]-0.2*x4[n]+x6[n]
        b1=(-0.1)*(x1[n]+x5[n])+0.2*x7[n]
        c1=(-0.1)*(x1[n]+x6[n]-x8[n])
        d1=-0.2*x7[n]

        k2=10*(x2[n]+j1*h/2-x1[n]-k1*h/2)+x4[n]+t1*h/2
        j2=76*(x1[n]+k1*h/2)-(x1[n]+k1*h/2)*(x3[n]+r1*h/2)+x4[n]+t1*h/2
        t2=-3*(x1[n]+k1*h/2+x2[n]+j1*h/2)+x5[n]+a1*h/2
        a2=-(x2[n]+j1*h/2)-0.2*(x4[n]+t1*h/2)+x6[n]+b1*h/2
        r2=(x1[n]+k1*h/2)*(x2[n]+j1*h/2)-x3[n]-r1*h/2-x4[n]-t1*h/2+x7[n]+c1*h/2
        c2=(-0.1)*(x1[n]+k1*h/2+x6[n]+b1*h/2-x8[n]-d1*h/2)

        k3=10*(x2[n]+j2*h/2-x1[n]-k2*h/2)+x4[n]+t2*h/2
        j3=76*(x1[n]+k2*h/2)-(x1[n]+k2*h/2)*(x3[n]+r2*h/2)+x4[n]+t2*h/2
        t3=-3*(x1[n]+k2*h/2+x2[n]+j2*h/2)+x5[n]+a2*h/2
        r3=(x1[n]+k2*h/2)*(x2[n]+j2*h/2)-x3[n]-r2*h/2-x4[n]-t2*h/2+x7[n]+c2*h/2


        k4=10*(x2[n]+j3*h/2-x1[n]-k3*h/2)+x4[n]+t3*h
        j4=76*(x1[n]+k3*h)-(x1[n]+k3*h)*(x3[n]+r3*h)+x4[n]+t3*h
        r4=(x1[n]+k3*h)*(x2[n]+j3*h)-x3[n]-r3*h-x4[n]-t3*h+x7[n]

        x1[n+1]=x1[n]+(h/6)*(k1+2*k2+2*k3+k4)
        x2[n+1]=x2[n]+(h/6)*(j1+2*j2+2*j3+j4)
        x3[n+1]=x3[n]+(h/6)*(r1+2*r2+2*r3+r4)

    x1=x1[3000:N]
    x1=np.abs(x1*100000000000000)
    x1 = np.floor(np.mod(np.abs(x1), 256))

    x2=x2[3000:N]
    x2=np.abs(x2*100000000000000)
    x2 = np.floor(np.mod(np.abs(x2), 256))

    x3=x3[3000:N]
    x3=np.abs(x3*100000000000000)
    x3 = np.floor(np.mod(np.abs(x3), 256))

    T=np.copy(img)

    if img_type=="G":
        n=0
        for i in range(height):
            for j in range(width):
                img[i,j]=x1[n]
                n+=1
    
    if img_type=="C":
        img2=np.copy(T)
        img3=np.copy(T)
        n=0
        for i in range(height):
            for j in range(width):
                img[i,j]=x1[n]
                img2[i,j]=x2[n]
                img3[i,j]=x3[n]
                n+=1
        img2=img2.astype(np.uint8)
        img3=img3.astype(np.uint8)
        

    img=img.astype(np.uint8)
    if img_type=="G":
        img = img ^ T.astype(np.uint8)
    else:
        result =[]
        for i in range(len(img)):
            row=[]
            for j in range(len(T[i])):
                pixel=[]
                for index,k in enumerate(T[i][j]):
                    if index==0:
                        bit = k ^img[i][j][0]
                    elif  index==1:
                        bit = k ^img2[i][j][0]
                    else:
                        bit = k ^img3[i][j][0]
                    pixel.append(bit)
                row.append(pixel)
            result.append(row)

        img = np.array(result).astype(np.uint8)
    
    img = img.astype(np.uint8)
    return img

# secret_key = "Mudkip"

# 
# encryptedImg = imgEncDec(image,secret_key)
# cv2.imwrite(f"./encrypted/enc_{imageName}",encryptedImg)


# decryptedImg = imgEncDec(encImg,secret_key)
# cv2.imwrite(f"./decrypted/dec_{imageName}",decryptedImg)
