from cryptography.fernet import Fernet
import hashlib
import base64
import pickle

# Convert string key to bytes using a hash function
def string_to_key(key_string):
    hashed_key = hashlib.sha256(key_string.encode()).digest()
    return base64.urlsafe_b64encode(hashed_key)

# Your string key
string_key = "mudkip"
key_bytes = base64.urlsafe_b64encode(hashlib.sha256(string_key.encode()).digest())
# Convert string key to bytes
# key_bytes = string_to_key(string_key)

# Initialize the cipher suite with the custom key
cipher_suite = Fernet(key_bytes)

# Example array
my_array =[[0,1,0,0,1,1],
          [0,0,1,1,0,1],
          [1,0,0,1,1,0],
          [1,1,1,0,0,0]]

# Serialize the array using pickle
serialized_array = pickle.dumps(my_array)

# Encrypt the serialized array
encrypted_data = cipher_suite.encrypt(serialized_array)

print("Encrypted data:", encrypted_data)

encrypted_data = b'gAAAAABmRIN7HPv6U-7ksLOo2A8quQvP3cYdSLHnCsx7lOYVMuwr-UZHgHoBZjIYKcg9guVmOX4e8STAk8hRyWeM2lA73EvR7X8okBQFfzOGvhuj7I-Ghcr76oweQY5Iv64geTRQxhg2c8_rGEIekHWJNaUsV_AloCkdScB7oKrpsVAGv41gh-WgLNjrUhBpnu7kTVgsBQ6B'
# Decrypt the encrypted data
decrypted_data = cipher_suite.decrypt(encrypted_data)

# Deserialize the decrypted data using pickle
decrypted_array = pickle.loads(decrypted_data)

print("Decrypted array:", decrypted_array)
