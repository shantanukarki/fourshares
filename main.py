import cv2
import numpy as np
from hyperchaaos import imgEncDec
from cryptography.fernet import Fernet
import hashlib
import base64
import pickle

img = cv2.imread("emma_stone.bmp")


string_key = "mudkip"
key_bytes = base64.urlsafe_b64encode(hashlib.sha256(string_key.encode()).digest())
cipher_suite = Fernet(key_bytes)
encrypted_data = b'gAAAAABmRIN7HPv6U-7ksLOo2A8quQvP3cYdSLHnCsx7lOYVMuwr-UZHgHoBZjIYKcg9guVmOX4e8STAk8hRyWeM2lA73EvR7X8okBQFfzOGvhuj7I-Ghcr76oweQY5Iv64geTRQxhg2c8_rGEIekHWJNaUsV_AloCkdScB7oKrpsVAGv41gh-WgLNjrUhBpnu7kTVgsBQ6B'
decrypted_data = cipher_suite.decrypt(encrypted_data)

# Deserialize the decrypted data using pickle
matrix = pickle.loads(decrypted_data)

# matrix = [[0,1,0,0,1,1],
#           [0,0,1,1,0,1],
#           [1,0,0,1,1,0],
#           [1,1,1,0,0,0]]


def shareGeneraator(image,title,loop):
    title = np.tile(title,loop)
    share = []
    for i,row in enumerate(image):
        shareRow = []
        for j,pixel in enumerate(title):
            #checking if the value is 1 in the share matrix and if it is addimng the corresponding pixel data from share image to the final image
            if pixel == 1:
                shareRow.append(image[i][j])
        share.append(shareRow)
    return share

def shareDecryptor(share,tile,loop,expandedShare):
    tile = np.tile(tile,loop)
    for i,row in enumerate(share):
        j=0
        for pixel in row:
            while tile[j]==0:
               j+=1
            if tile[j]==1:
                expandedShare[i][j]=pixel
                j+=1
    return expandedShare
                

def fourShares(image,matrix,key):
    matrix = np.array(matrix)
    image = np.array(image)
    h,w,c = image.shape
    #checking if the length of the image is divisible by 6 if not then adding columns of zeros to make it a multiple of 6 
    if w%6 !=0:
        num_zeroes = w%6
        zeroes_array = np.zeros((h, (6-num_zeroes),c), dtype=image.dtype)
        image = np.concatenate((image, zeroes_array), axis=1)

    loop = int(len(image[0])/6)
    #creating the 4 shares with the given matrix
    s1 = np.array(shareGeneraator(image,matrix[0],loop),dtype=image.dtype)
    s2 = np.array(shareGeneraator(image,matrix[1],loop),dtype=image.dtype)
    s3 = np.array(shareGeneraator(image,matrix[2],loop),dtype=image.dtype)
    s4 = np.array(shareGeneraator(image,matrix[3],loop),dtype=image.dtype)
    s1 = imgEncDec(s1,key)
    s2 = imgEncDec(s2,key)
    s3 = imgEncDec(s3,key)
    s4 = imgEncDec(s4,key)
    return(s1,s2,s3,s4)


(s1,s2,s3,s4) = fourShares(img,matrix,"Mudkip")

cv2.imwrite('s1.bmp',s1)
cv2.imwrite('s2.bmp',s2)
cv2.imwrite('s3.bmp',s3)
cv2.imwrite('s4.bmp',s4)

def shareCombiner(pattern,matrix,key):
    #reading the share images according to the user input
    share1 = cv2.imread(f's{pattern[0]}.bmp')
    share2 = cv2.imread(f's{pattern[1]}.bmp')
    share3 = cv2.imread(f's{pattern[2]}.bmp')
    #decrypting the image
    share1 = imgEncDec(share1,key)
    share2 = imgEncDec(share2,key)
    share3 = imgEncDec(share3,key)
    m1,m2,m3= (matrix[pattern[0]-1]),(matrix[pattern[1]-1]),(matrix[pattern[2]-1])
    h,w,c = share1.shape
    imgW = w *2
    #the time we have to multiply/loop the matrix to make it the same size as the image
    loop = int(imgW/6)
    #crating a empty array to store the combined image output
    expandedShare = np.zeros((h,imgW,c),dtype=share1.dtype)

    #adding the data from each shares into one single array
    expandedShare = shareDecryptor(share1,m1,loop,expandedShare)
    expandedShare = shareDecryptor(share2,m2,loop,expandedShare)
    expandedShare = shareDecryptor(share3,m3,loop,expandedShare)

    for i in range(5):
        if np.sum(expandedShare[:,-1]) == 0 :
            expandedShare = np.delete(expandedShare,-1,axis=1)
        else:
            break
    return expandedShare

    
#the first parameter is an array containing the nimber of three shares that you want to decrypt
decImage = shareCombiner([1,3,2],matrix,"Mudkip")

cv2.imwrite('decImage.bmp',decImage)